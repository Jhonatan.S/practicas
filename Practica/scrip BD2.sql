create database bd2;
use bd2;
create table jefes(
id_jefe int not null primary key auto_increment,
nombre varchar(100) not null
);
create table empleados(
id_empleado int not null primary key auto_increment,
primer_nombre varchar(50) not null,
segundo_nombre varchar(50) not null,
primer_apellido varchar(50) not null,
segundo_apellido varchar(50) not null,
correo varchar(150) not null,
tel_fijo varchar(9) not null,
tel_movil varchar(9) not null,
jefe int not null,
constraint jefes foreign key (jefe) references jefe(id_jefe)
);