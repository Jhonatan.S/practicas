package confs;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author jhonatan.acevedousam
 */
public class Conexion {
    
    public DriverManagerDataSource conectar(){
        DriverManagerDataSource s =  new DriverManagerDataSource();
        s.setDriverClassName("com.mysql.jdbc.Driver");
        s.setUrl("jdbc:mysql://localhost:3306/vivienda");
        s.setUsername("root");
        s.setPassword("root");
        return s;
    }
    
    public void desconectar(){
        
    }
}
