/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.ClienteDao;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Cliente;

@ManagedBean
@SessionScoped
public class ClienteControl implements Serializable {
    
    private Cliente ci;
    private ClienteDao cid;
    private List<Cliente> lista;

    public Cliente getCi() {
        return ci;
    }

    public void setCi(Cliente ci) {
        this.ci = ci;
    }

    public List<Cliente> getLista() {
        return lista;
    }

    public void setLista(List<Cliente> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init(){
    ci = new Cliente();
    consultar();
    }
    
    public void consultar(){
    lista = cid.findAll();
    }
    
    public void insertar(){
        try {
            cid.create(ci);
            
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
        }
    }
}
