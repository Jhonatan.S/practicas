/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Conexion {
    
    private static Connection conn;
    
    public static Connection conectar(){
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/prueba?useSSL=false", "root", "root");
            
            PreparedStatement p = conn.prepareStatement("select * from servicios order by id");
            ResultSet r = p.executeQuery();
        } catch (SQLException e) {
            System.out.println("Error "+e.getMessage());
        }
        return conn;
    }
    
    public static void cerrar(){
        try {
            if (conn != null) {
                if (!conn.isClosed()) {
                    conn.close();
                }
            }
        } catch (SQLException e) {
            System.out.println("Error "+e.getMessage());
        }
    }
}
