<%@taglib prefix="k" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <div><a href="add.html">+</a></div>
        <div>
            <table border="0">
                <thead>
                    <tr>
                        <th>HABITANTES</th>
                        <th>DIRECCION</th>
                    </tr>
                </thead>
                <tbody>
                    <k:forEach var="reg" items="${listVivienda}">
                        <tr>
                            <td>${reg.habitantes}</td>
                            <td>${reg.direccion}</td>
                        </tr>
                    </k:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
