package Control;

import Model.Vivienda;
import confs.Conexion;
import dao.DVivienda;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author jhonatan.acevedousam
 */
@Controller
public class Viviendas {

    private DVivienda daoVivi = new DVivienda();
    private ModelAndView vi = new ModelAndView();
    private Conexion conn = new Conexion();
    private JdbcTemplate jtem = new JdbcTemplate(conn.conectar());
    private String[] viviendas = {"select habitantes,direccion from viviendas limit 4",
        "insert into viviendas(habitantes,direccion) values(?,?)",
        "select * from viviendas where id="};

    @RequestMapping(value = "add.html", method = RequestMethod.GET)
    public ModelAndView ingresarVivienda() {
        vi.addObject(new Vivienda());
        vi.setViewName("add");
        return vi;
    }

    @RequestMapping(value = "add.html", method = RequestMethod.POST)
    public ModelAndView ingresarVivienda(Vivienda v) {
        return daoVivi.registarVivienda(v);
    }

    @RequestMapping("index.html")
    public ModelAndView consultarVivienda() {
        return daoVivi.consultarVivienda();
    }
}
