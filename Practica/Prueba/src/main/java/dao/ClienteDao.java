/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import modelo.Cliente;
import util.Dao;

public class ClienteDao implements Dao<Cliente> {

    Connection conn;

    private PreparedStatement p;
    private String[] ar = {
        "insert into clientes (nombres,apellido,email,telefono) values(?,?,?,?)",
        "Select * from clientes order by id",
        "update clientes set nombre=?, apellido=?, email=?, telefono=?, where id=?",
        "delete from clientes where id=?"
    };

    private Cliente ci;
    private List<Cliente> lista;
    
    @Override
    public void create(Cliente t) {
        try {
            p = conn.prepareStatement(ar[0]);
            p.setString(1, ci.getNombres());
            p.setString(2, ci.getApellido());
            p.setString(3, ci.getEmail());
            p.setString(4, ci.getTelefono());
            p.executeUpdate();
            
            System.out.println("Exito al guardar");
        } catch (Exception e) {
            System.out.println("Error al guardar "+e.getMessage());
        }
    }

    @Override
    public void update(Cliente t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Cliente t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cliente> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
