package dao;

import Model.Vivienda;
import confs.Conexion;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author jhonatan.acevedousam
 */
public class DVivienda {
    
    private String [] viviendas = {"select habitantes,direccion from viviendas order by id desc limit 7",
    "insert into viviendas(habitantes,direccion) values(?,?)",
    "select habitantes,direccion from viviendas where id=",
    "update viviendas set habitantes=?, direccion=? where id=?",
    "delete from viviendas where id="};
    
    Vivienda v = new Vivienda();
    private Conexion conn = new Conexion();
    private JdbcTemplate jtem = new JdbcTemplate(conn.conectar());
    private ModelAndView mav = new ModelAndView();
    private List listVivienda;
    
    /* Consultar Vivienda*/
    public ModelAndView consultarVivienda(){
    listVivienda = this.jtem.queryForList(viviendas[0]);
    mav.addObject("listVivienda", listVivienda);
    mav.setViewName("index");
    return mav;
    }
    
    /* Registrar Vivienda*/
    public ModelAndView registarVivienda(Vivienda v){
    
        this.jtem.update(viviendas[1],v.getHabitantes(),v.getDireccion());
        mav.setViewName("redirect:/index.html");
        return mav;
    }
}
