use bd1;

INSERT INTO bd1.jefe VALUES(0,'Jhonatan Stanley Acevedo Monrroy');
INSERT INTO bd1.jefe VALUES(0,'Tamara Sofia Lopez Granados');
INSERT INTO bd1.jefe VALUES(0,'Fredy Antonio Alfaro Santos');
INSERT INTO bd1.jefe VALUES(0,'Militza Landaverde Diaz');


INSERT INTO bd1.empleados VALUES(0,'Juan','Carlos','Perez','Torres','juan_torres@gmail.com','2236-5210','7896-4563',1);
INSERT INTO bd1.empleados VALUES(0,'Marco','Antonio','Ramos','Ayala','marcor@hotmail.com','2210-5120','7963-1520',1);
INSERT INTO bd1.empleados VALUES(0,'Sabrina','Alessandra','Cruz','Martinez','alessa.martinez@yahoo.com','2232-1020','7277-2015',1);
INSERT INTO bd1.empleados VALUES(0,'Jose','Antonio','Madero','Barrera','pepe.madero@gmail.com','2230-4563','7896-5210',2);
INSERT INTO bd1.empleados VALUES(0,'Oscar','Alejandro','Acevedo','Cruz','os_acevedo@outlook.com','2645-9874','7498-4526',2);
INSERT INTO bd1.empleados VALUES(0,'Sofia','Saori','Acevedo','Lopez','ss_lopez@gmail.com','2296-7192','7430-8270',4);
INSERT INTO bd1.empleados VALUES(0,'Axel','Alessandro','Lopez','Cruz','axel.lopez@yahoo.com','2236-9856','7963-5210',3);